from mecplan import Crawler, CsvConverter
import os
import ntpath
import json

import csv
import xlrd
import time

# 0. Script Setup
directory = "\\\\MILFPSP01110\\GRM-Italy$\\MEC\\MEC-DB" # directory = 'I:\\MEC\\MEC-DB'

file_dir = os.path.dirname(os.path.realpath(__file__))
output_dir = os.path.join(file_dir, 'output')

extension = '[!~]*.xlsm' # Excluding open temp files

# 1. Crawl over the network drive
if not os.path.exists(directory):
    raise ValueError(directory + ' is not valid directory')

start_time = time.time()
c = Crawler()
result = c.execute(directory, extension)

with open(os.path.join(file_dir,'files_summary.json'), 'w') as f:
    f.write(json.dumps(result, indent=1))

parse_time = time.time()
print("--- {:.2f} seconds to collect plans ---".format(parse_time - start_time))

# 2. convert XLSM to CSV
cc = CsvConverter(files_dict=result, output_dir=output_dir)
cc.execute()

conversion_time = time.time()
print("--- {0:.2f} seconds to convert {1:d} plans ---".format(
    (conversion_time - parse_time), len(result.keys())
))
