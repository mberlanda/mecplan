import os
import csv
from mecplan import Linter
from glob import glob
import time

MEGATAB_HEADERS = [
    "CLIENTE", "PIANO", "DATA_SCRITTURA", "OBIETTIVO", "SITO", "CONCESSIONARIA", "PLACEMENT", "FORMATO",
    "SOGGETTO", "DATA_PIANIFICAZIONE", "CPx_LORDO", "SCONTO", "CPx_NET", "CPx_NETNET", "CPM_CPA", 
    "TOT_LORDO", "TOT_NET", "TOT_NETNET", "NOTE_PIANO", "CATEGORIA_FORMATO", "NOTA_FORMATO", 
    "POST_VALUTAZIONE", "TRACKING", "SETTIMANA", "MESE", "ANNO", "UTENTE", "PATH", "CAMPAGNA", "SEZIONE", 
    "QUANTITA", "SOTTOCATEGORIA_FORMATO"
]

start_time = time.time()

file_dir = os.path.dirname(os.path.realpath(__file__))
output_dir = os.path.join(file_dir, 'output')

files = glob(os.path.join(output_dir, "*.csv"))

timestamp = int(time.time())
with open(os.path.join(file_dir, "{0}_megatab.csv".format(timestamp)), 'w', newline='') as megatab:
    megatab_writer = csv.writer(megatab, delimiter=';', quoting=csv.QUOTE_ALL)
    # writer.writerow(MEGATAB_HEADERS)
    megatab_writer.writerow(MEGATAB_HEADERS)
    with open(os.path.join(file_dir, "{0}_techdb.csv".format(timestamp)), 'w', newline='') as techdb:
        techdb_writer = csv.writer(techdb, delimiter=';', quoting=csv.QUOTE_ALL)      
        techdb_writer.writerow(MEGATAB_HEADERS)
        for f in files:
            # print(f)
            l = Linter(f)
            data = [d for d in l.execute() if not d[5] == 'CONCESSIONARIA']
            megatab_writer.writerows([d for d in data if d[7] and not d[14] == 'CT'])
            techdb_writer.writerows([d for d in data if not d[7] and d[14] == 'CT'])
            
   
print("--- {:.2f} seconds to combine all files ---".format(time.time() - start_time))

    