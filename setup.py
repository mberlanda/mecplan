from setuptools import setup

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='mecplan',
      version='0.1',
      description='MecPlan utilities',
      url='',
      author='mberlanda',
      author_email='mauro.berlanda@gmail.com',
      install_requires=[],
      license='MIT',
      packages=['mecplan'],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose'])
