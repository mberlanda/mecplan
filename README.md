# MecPlan

## Development

Summary:

- [Prepare your development environment on Windows](#prepare-your-development-environment-on-windows)
- [Initialize your virtual environment](#initialize-your-virtual-environment)
- [Setup this package](#setup-this-package)
- [Try the code](#try-the-code)
- [Connecting to the Database](#connecting-to-the-database)

### Prepare your development environment on Windows

**Install Python**

Download the [latest Python 3 Release](https://www.python.org/downloads/windows/) from the official website.

If you try to install it from the executable you will get the following error:

> The TARGETDIR variable must be provided when invoking this installer

- Right-click on the installer and _Run as Administrator_
- Custom Install > Create the path `C:\Python36` and select it as a target for the installation
- Tick the following options:
  - Associate files with Python
  - Create shortcuts for installed applications
  - Add Python to environment variables

Now open the `cmd` prompt to check that the installation went fine:

```
C:\Users\your.username>pip -V
pip 9.0.1 from c:\python36\lib\site-packages (python 3.6)

C:\Users\your.username>python -V
Python 3.6.3
```

**Basic Python Packages**

Install a few python packages you will always need, such as:

```
C:\Users\your.username>pip install virtualenv
Collecting virtualenv
  Downloading virtualenv-15.1.0-py2.py3-none-any.whl (1.8MB)
    100% |████████████████████████████████| 1.8MB 607kB/s
Installing collected packages: virtualenv
Successfully installed virtualenv-15.1.0
```

**Install A Text Editor or an IDE**

My favourite text editor is **Sublime** and you can download it [from here](https://www.sublimetext.com/).
If you prefer to work with an IDE, there is a plenty of good options with pros and cons:

- [PyCharm Community](https://www.jetbrains.com/pycharm/): this is the most complete free solution but it will drain the resources of your computer
- [Wing Personal](https://wingware.com/downloads/wingide-personal): this IDE has less features but it is very helpful for code autocompletion

**Install and configure Git for Windows**

Assuming that your are going to keep working under version control, download [Git for Windows](https://git-for-windows.github.io/). Select the option _Use Git from the Windows Command Prompt_.

Edit the git config file generated in `C:\ProgramData\Git` (hidden folder):

  - Right-click on `config` and Open with Sublime
- The default config should look like:
```
  [core]
    symlinks = false
    autocrlf = true
    fscache = true
  [color]
    diff = auto
    status = auto
    branch = auto
    interactive = true
  [help]
    format = html
  [rebase]
    autosquash = true
```
- Open the `C:\Users\your.name\.gitconfig` file and paste:
```
[user]
  name = Your Name
  email = your@email.com
[alias]
  st = status
  co = checkout
  br = branch
  up = rebase
  ci = commit
  aa = add --all
[push]
  default = simple
```

> Alternatively you add this global config from the `git bash` (You will probably need to add at least one value since `.gitconfig` is not a valid name if you want to create a new file under Windows):

```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

Finally, verify your configuration from `git bash` running:
```
your.name@MACHINE-NAME MINGW64 ~
$ git config --list
core.symlinks=false
core.autocrlf=true
core.fscache=true
color.diff=auto
color.status=auto
color.branch=auto
color.interactive=true
help.format=html
rebase.autosquash=true
user.name=Your name
user.email=your@email.com
alias.st=status
alias.co=checkout
alias.br=branch
alias.up=rebase
alias.ci=commit
alias.aa=add --all
push.default=simple
http.sslbackend=openssl
diff.astextplain.textconv=astextplain
filter.lfs.clean=git-lfs clean -- %f
filter.lfs.smudge=git-lfs smudge -- %f
filter.lfs.process=git-lfs filter-process
filter.lfs.required=true
credential.helper=manager
```

**Create an RSA key pair**

You can refer to this [blog post](http://inchoo.net/dev-talk/how-to-generate-ssh-keys-for-git-authorization/).

Open the `git bash` and run:

```
$ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/your.name/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /c/Users/your.name/.ssh/id_rsa.
Your public key has been saved in /c/Users/your.name/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:{passphrase} your.name@MACHINE-NAME
The key's randomart image is:
+---[RSA 2048]----+
|       .o.o.     |
|         +.=     |
|       ...= o    |
|     .o .= + =   |
|    .o.oS * O o .|
|   .oo.*=..@ . o |
|   .o oo==o = .  |
|     .  +o . o   |
|         oE .    |
+----[SHA256]-----+
```

The key pair will is now generated under `C:\Users\your.name\.ssh\`


**Create an Account on Bitbucket**

- Go to [https://bitbucket.org/account/signin/](https://bitbucket.org/account/signin/), register and create a new account.
- Go to your profile page (https://bitbucket.org/your-name/)
- Click on Settings
- Under Security > SSH Keys > Add Key

Since we have already created a key pair in the previous step we will open `.ssh/id_rsa.pub` with a text editor - I would use Sublime, copy the content (! it should be one line only) and paste it.

**Git Aware Prompt**
Assuming that you are working under version control, it would be very useful to know in which branch you are located. In order to achieve it, you can install [Git Aware Prompt](https://github.com/jimeh/git-aware-prompt) on UNIX.

In Windows, you can simply use `git bash` as a terminal.


**Clone this project**

Create under `Documents` the `Projects` folder.

From the `cmd` prompt:

```
C:\Users\your.username>cd Documents
C:\Users\your.username\Documents>cd Projects
C:\Users\your.username\Documents\Projects>git clone git@bitbucket.org:mberlanda/mecplan.git
Cloning into 'mecplan'...
remote: Counting objects: 54, done.
remote: Compressing objects: 100% (48/48), done.
remote: Total 54 (delta 10), reused 0 (delta 0)
Receiving objects: 100% (54/54), 230.40 KiB | 402.00 KiB/s, done.
Resolving deltas: 100% (10/10), done.
```

### Initialize your virtual environment

On Windows:
```
C:\Users\your.name\Documents\Projects\mecplan>virtualenv .venv
Using base prefix 'c:\\python36'
New python executable in C:\Users\your.name\Documents\Projects\mecplan\.venv\Scripts\python.exe
Installing setuptools, pip, wheel...done.
C:\Users\your.name\Documents\Projects\mecplan>.\.venv\Scripts\activate
(.venv) C:\Users\your.name\Documents\Projects\mecplan>
```

On Unix:
```
$ virtualenv -p python3 .venv
$ source .venv/bin/activate
(.venv) $
```

Note that if you are using an IDE, you should set the python path to the one included in your `.venv` folder (e.g. [Wing Instructions](https://wingware.com/doc/howtos/virtualenv))

### Setup this package
```
# Install required packages
(.venv) $ pip install -r requirements.txt
# Install mecplan dist with dependencies
(.venv) $ pip install -e .
# Run mecplan tests
(.venv) $ python setup.py test
```

### Try the code
```
(.venv) $ python mecplan/main.py
```

### Connecting to the Database

- [check out this blog post](https://tryolabs.com/blog/2012/06/25/connecting-sql-server-database-python-under-ubuntu/)
- [so question](https://stackoverflow.com/questions/16024956/connecting-to-sql-server-with-pypyodbc)
