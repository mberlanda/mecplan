from mecplan import Crawler
import os
import ntpath
import json

import csv
import xlrd

file_dir = os.path.dirname(os.path.realpath(__file__))

directory = os.path.join(file_dir, 'input')
output_dir = os.path.join(file_dir, 'output')
extension = '*'

if __name__ ==  '__main__':
    c = Crawler()
    files = c.glob_dir(directory, extension)
    result = c.process_files(files)
    print(json.dumps(result, indent=1))

    keys = result.keys()
    for k in keys:
        fp = result[k]['full_path']
        workbook = xlrd.open_workbook(fp)
        for sheet in workbook.sheets():
            if sheet.name == 'DB':
                with open(os.path.join(output_dir, '{}.csv'.format(k)), 'w') as f:
                    writer = csv.writer(f, quoting=csv.QUOTE_ALL)
                    writer.writerows(sheet.row_values(row) for row in range(sheet.nrows))
 
  