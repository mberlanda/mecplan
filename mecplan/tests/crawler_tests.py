from mecplan import Crawler

from unittest import TestCase
import os

class CrawlerTests(TestCase):

    def setUp(self):
        self.file_dir = os.path.dirname(os.path.realpath(__file__))
        self.fixtures_dir = os.path.join(self.file_dir, 'fixtures')
        self.obj = Crawler()

    def test_glob_dir_all(self):
        self.assertEqual(
            len(self.obj.glob_dir(self.fixtures_dir, '*')),
            3
        )

    def test_glob_dir_xslm(self):
        self.assertEqual(
            len(self.obj.glob_dir(self.fixtures_dir, '*.xlsm')),
            3
        )

    def test_glob_dir_rb(self):
        self.assertEqual(
            len(self.obj.glob_dir(self.fixtures_dir, '*.rb')),
            0
        )

    def test_cleanup_version(self):
        fixtures = ['plan1_v1', 'plan1_v2', 'plan2_v1']
        expected = [ ('plan1', 1), ('plan1', 2), ('plan2', 1) ]
        self.assertEqual(
            [ Crawler.cleanup_version(f) for f in fixtures ],
            expected
        )

    def test_remove_extension(self):
        self.assertEqual(
            Crawler.remove_extension('plan1_v1.xlsm'),
            'plan1_v1'
        )

    def test_process_file_basename(self):
        self.assertEqual(
            Crawler.process_file_basename('plan1_v5.xlsm'),
            ('plan1', 5)
        )

    def test_process_files(self):
        raw_files = self.obj.glob_dir(self.fixtures_dir, '*.xlsm')
        actual = self.obj.process_files(raw_files)

        self.assertEqual(len(raw_files), 3)
        self.assertEqual(len(actual.keys()), 2)

        self.assertEqual(sorted(actual.keys()), ['plan1', 'plan2'])
        self.assertEqual(actual['plan1']['version'], 2)
        self.assertEqual(actual['plan2']['version'], 1)
        self.assertEqual(
            sorted(actual['plan1'].keys()),
            ['basename', 'full_path', 'version']
        )
