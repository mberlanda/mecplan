from mecplan import CsvConverter, Crawler

from unittest import TestCase
import os
import pdb

class CsvConverterTests(TestCase):

    def setUp(self):
        self.file_dir = os.path.dirname(os.path.realpath(__file__))
        self.fixtures_dir = os.path.join(self.file_dir, 'fixtures')
        self.output_dir = os.path.join(self.file_dir, 'output')
        self.crawler = Crawler()
        self.files = self.crawler.execute(self.fixtures_dir, '*.xlsm')

    def test_raise_exception_without_args(self):
        with self.assertRaisesRegex( Exception,
            'should provide files_dict= and output_dir= kwargs',
        ):
            CsvConverter()

    def test_raise_exception_with_one_arg(self):
        with self.assertRaisesRegex( Exception,
            'should provide files_dict= and output_dir= kwargs',
        ):
            CsvConverter(files_dict={})

    def test_no_exception_with_args(self):
        files_dict = {
            'a_key' : {
                'full_path': 'fp',
                'version': 1,
                'basename': 'a_key_v1'
            }
        }
        cc = CsvConverter(files_dict=files_dict, output_dir='/')
        self.assertTrue(isinstance(cc, CsvConverter))


    def test_files(self):
        cc = CsvConverter(
            files_dict=self.files,
            output_dir=self.output_dir
        )
        cc.execute()

