from mecplan import Crawler
import os
import json

file_dir = os.path.dirname(os.path.realpath(__file__))

if __name__ ==  '__main__':
    filename = os.path.join(file_dir, 'file_list.txt')
    lines = [line.rstrip('\n') for line in open(filename, encoding='latin-1')]
    print("{} lines found".format(len(lines)))

    result = Crawler().process_files(lines)
    print("{} unique plans detected".format(len(result.keys())))

    with open(os.path.join(file_dir,'file_list.json'), 'w') as f:
        f.write(json.dumps(result, indent=1))
