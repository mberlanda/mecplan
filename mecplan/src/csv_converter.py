import xlrd
import os
import csv

class CsvConverter():

    def __init__(self,files_dict=None, output_dir=None):
        if not files_dict or not output_dir:
            raise Exception('You should provide files_dict= and output_dir= kwargs')
        self.files_dict = files_dict
        self.output_dir = output_dir
        return

    def execute(self):
        keys = self.files_dict.keys()
        [ self.process_single_file(k) for k in keys ]

    def process_single_file(self, key):
        fp = self.files_dict[key]['full_path']
        try:
            workbook = xlrd.open_workbook(fp)
            self.process_wb(key, workbook)
        except xlrd.biffh.XLRDError:
            pass

    def process_wb(self, key, workbook):
        sheets = filter(lambda x: x.name == 'DB', workbook.sheets())
        [ self.write_csv(key, sheet) for sheet in sheets ]

    def write_csv(self, key, sheet):
        with open(os.path.join(self.output_dir, '{}.csv'.format(key)), 'w', newline='') as f:
            writer = csv.writer(f, quoting=csv.QUOTE_ALL, delimiter=';')
            writer.writerows(sheet.row_values(row) for row in range(sheet.nrows))
