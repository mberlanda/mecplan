import csv
import xlrd
import datetime

class Linter():

    DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
    DATE_FORMAT = '%Y-%m-%d'

    def __init__(self, file_path):
        self.fp = file_path
        self.file_to_list()
        return

    def execute(self):
        return [ self.clean_line(f) for f in self.list[1:] ]


    def file_to_list(self):
        with open(self.fp, 'r') as f:
            reader = csv.reader(f, delimiter=';', quoting=csv.QUOTE_ALL)
            self.list = list(reader)

    def clean_line(self, row):
        row[2] = Linter.format_datetime(row[2])
        for i in range(3,10): row[i] =  Linter.trim_str(row[i])
        row[10] = Linter.format_date(row[10])
        for i in range(11,15): row[i] = Linter.format_float(row[i])
        for i in range(16,20): row[i] = Linter.format_float(row[i])
        for i in range(20,25): row[i] =  Linter.trim_str(row[i])
        row[45] = Linter.format_float(row[45])  
        for i in range(46,48): row[i] = int(Linter.format_float(row[i]))
        
        indexes = [
            0, 1, 2, 3, 6, 5, 24, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18,
            19, 20, 21, 23, 25, 26, 45, 46, 47, 48, 49, 4, 7, 16, 22
        ]
        return [ row[i] for i in indexes ]

    @staticmethod
    def format_float(x):
        try:
            return float(x.replace(",", ".") or 0)
        except ValueError:
            return 0.0
    @staticmethod
    def format_datetime(x):
        return Linter.parse_datetime(x).strftime(Linter.DATETIME_FORMAT)

    @staticmethod
    def format_date(x):
        return Linter.parse_datetime(x).strftime(Linter.DATE_FORMAT)

    @staticmethod
    def parse_datetime(x):
        try:
            y = float(x)
            if y > 100000:
                y = float(str(y)[:5])
            return xlrd.xldate_as_datetime(float(y), 0)
    
        except ValueError:
            return datetime.datetime(9999,12,31,23,59,59)

    @staticmethod
    def trim_str(x):
        return x.replace("\n", " ").replace("\r", "")