from glob import glob
from os import path
import ntpath
import re

class Crawler():

    VERSION_PATTERN = re.compile(r"[\-_]v\d+", re.I)

    def __init__(self, verbose=True):
        self.verbose = verbose
        return

    def execute(self, directory, extension):
        return self.process_files(self.glob_dir(directory, extension))

    def process_files(self, file_list):
        if self.verbose: print("processing {:d} files".format(len(file_list)))
        self.files = {}
        for f in file_list:
            basename = ntpath.basename(f)
            key_name, version = Crawler.process_file_basename(basename)
            if key_name in self.files.keys():
                if self.files[key_name]['version'] < version:
                    self.files[key_name] = {
                        'version': version,
                        'basename': basename,
                        'full_path': f
                    }
            else:
                self.files[key_name] = {
                    'version': version,
                    'basename': basename,
                    'full_path': f
                }
        if self.verbose: print("{:d} unique plans detected".format(len(self.files.keys())))
        return self.files

    def glob_dir(self, path_name, ext='*'):
        return glob(path.join(path_name, ext))

    @staticmethod
    def process_file_basename(file_name):
        key_name = Crawler.remove_extension(file_name)
        clean_name, version = Crawler.cleanup_version(key_name)
        return clean_name, version

    @staticmethod
    def cleanup_version(file_name):
        version_str = re.findall(Crawler.VERSION_PATTERN, file_name)[0]
        version_int = int(re.sub(r"\D", "", version_str))
        clean_name = file_name.replace(version_str, '')
        return (clean_name, version_int)

    @staticmethod
    def remove_extension(file_name):
        return ''.join(file_name.split('.')[:-1])
